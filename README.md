# Zoom Subtitles Improver

This project was coded during my 2022-2023 internship at EmployAbility, a not-for-profit helping disabled people get the adjustments they need in the workplace, and disabled students and graduates in their transition from education to employment. EmployAbility works with a number of major organisations including Google, Amazon, Thoughtworks, the FCA, Bloomberg, JP Morgan, Aspect Capital, and Schroder's.

The primary purpose of this Flask app is to improve the accuracy of Zoom's auto-generated subtitles. It was needed because there was a lot of manual work in the current process of subtitling EmployAbility presentation videos.

It improves subtitles by calculating the Levenshtein distance between: each word/phrase in a list; and all word-level n-grams (up to a certain threshold) of a text document. If a substitution threshold is met, it corrects the word(s) in the text with the word/phrase in the list.

Then, it checks for collisions (ie, if overlapping words would be substituted), and decides which substitution is the best to make.

It also has a review function, where you can set a higher substitution threshold that flags a word(s) for manual review, and then decide whether to substitute it or not. Finally, it can do character-level n-grams rather than word-level ones, but I have not found that to work very well for this usecase.

In addition to the main app I just described (app.py & subtitles_improver.py), Mara Yap did most of the work on file conversions between different types of subtitle files (text, WebVTT, SubRip).

I discontinued work on this project, because I and the company's tech lead realised that [Whisper](https://github.com/openai/whisper) is much more effective for generating subtitles, and [Whisper C++](https://github.com/ggerganov/whisper.cpp) is quick enough for everyday local use. I managed to fiddle around with Whisper C++ to get it to work on an M1 Mac with Core ML acceleration enabled, and tweaked the parameters until we were satisfied.
