# a program to improve subtitles, or: multi-dimensional lists hurt my brain

from Levenshtein import distance

def ngramify(word_level_ngrams, text, n):
    # input: 'The quick brown fox jumps', 2
    # output: ['The quick', 'quick brown', 'brown fox']
    if word_level_ngrams:
        ngrams = []
        text_split = text.split()

        for i in range(len(text_split) - n + 1):
            appendable = ''

            for j in range(n):
                appendable += text_split[i + j]

                # add spaces between words if they're not the last word in the current
                # group of n
                if j < n - 1:
                    appendable += ' '

            ngrams.append(appendable)

        return ngrams

    # input: 'The quick brown fox', 2
    # output: ['Th', 'he', 'e ', ' q', 'qu', 'ui', 'ic', 'ck', 'k ', ' b', 'br', 'ro', 'ow', 'wn', 'n ', ' f', 'fo', 'ox']
    else:
        return [text[i : i + n] for i in range(len(text) - n + 1)]

'''
word-level
- input: ['The quick', 'quick brown', 'brown fox']
- output: 'The quick brown fox'

- input: ['The quick brown', 'quick brown fox', 'brown fox jumps']
- output: 'The quick brown fox'

character-level
- input: ['Th', 'he', 'e ', ' q', 'qu', 'ui', 'ic', 'ck', 'k ', ' b', 'br',
  'ro', 'ow', 'wn', 'n ', ' f', 'fo', 'ox' 'x']
- output: 'The quick brown fox'

- input: ['The', 'he ', 'e q', ' qu', 'qui', 'uic', 'ick', 'ck ', 'k b', ' br',
  'bro', 'row', 'own', 'wn ', 'n f', ' fo', 'fox']
- output: 'The quick brown fox'
'''
def un_ngramify(word_level_ngrams, ngrams):
    text = ngrams[0]

    for i in range(1, len(ngrams)):
        text += ' ' + ngrams[i].split()[-1] if word_level_ngrams else ngrams[i][-1]

    return text

'''
different cases

collision: d2_i1 <= d1_i1 <= d2_i2
****__****
***__*****

***__***
**____**

collision: d1_i1 <= d2_i1 <= d1_i2
****__****
*****__***

**____**
***__***

no collision
******__**
****__****

**__******
****__****
'''
def ngrams_collide(dict_1, dict_2):
    d1_i1 = dict_1['index_1']
    d1_i2 = dict_1['index_2']
    d2_i1 = dict_2['index_1']
    d2_i2 = dict_2['index_2']

    if d2_i1 <= d1_i1 and d1_i1 <= d2_i2:
        return True
    elif d1_i1 <= d2_i1 and d2_i1 <= d1_i2:
        return True
    else:
        return False

def output_message(type, dict_1, dict_2=None, line=None):
    if type == 'TO SUBSTITUTE':
        print("TO SUBSTITUTE '" + dict_1['correction'] + "' FOR '" + \
              dict_1['ngram'] + "'")
    elif type == 'COLLISION':
        print("COLLISION BETWEEN '" + dict_1['ngram'] + "' AND '" + \
              dict_2['ngram'] + "': REMOVE SUBSTITUTION '" + \
              dict_1['correction'] + "' FOR '" + dict_1['ngram'] + "'")
    elif type == 'REVIEW':
        print("REVIEW COLLISION")

        review_line_1 = line[: dict_1['index_1']] + dict_1['correction'] + \
                             line[dict_1['index_2'] + 1 :]
        review_line_2 = line[: dict_2['index_1']] + dict_2['correction'] + \
                             line[dict_2['index_2'] + 1 :]

        '''
        highlight words on edges of ngram — clearer
        |quick|c -> |quickc|
        c|quick| -> |cquick|
        '''
        offset_1 = len(review_line_1) - len(line)
        offset_2 = len(review_line_2) - len(line)

        # bold from start of word
        review_line_1 = line[: dict_1['index_1'] + offset_1] + '\033[1m' + line[dict_1['index_2'] + offset_1:]

        # bold to end of word
        review_line_2 = line[: dict_2['index_1'] + offset_2] + '\033[0m' + line[dict_2['index_2'] + offset_2:]

        print('OPTION 1:', review_line_1)
        print('OPTION 2:', review_line_2)
        print('OPTION 3:', line)
    elif type == 'SUBSTITUTED':
        print("SUBSTITUTED '" + dict_1['correction'] + "' FOR '" + \
              dict_1['ngram'] + "'")
    else:
        print('error in output_message')

def correct_text(word_level_ngrams, substitution_threshold, review_threshold, \
                 review_collisions, text, corrections):
    '''
    dictionaries[][][] list structure: 1D for each word, 2D for each pass, 3D for dictionary containing levenshtein distance

    eg (only levenshtein distances shown)
    [[[4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, ....], [9, 8, 7, 6, 5, 4, ....], [2, 3, 4, 5, 6, 7, ....]],
    [[4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, ....], [9, 8, 7, 6, 5, 4, ....], [2, 3, 4, 5, 6, 7, ....]],
    [[4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, ....], [9, 8, 7, 6, 5, 4, ....], [2, 3, 4, 5, 6, 7, ....]]]
    where
    - first row is first word, second row is second word, etc
    - first list of each row is first pass, second list is second pass, etc
    - each pass is a list of levenshtein distances
    '''

    text_to_correct = text
    substitutions_count = 0
    passes = len(text.split()) if word_level_ngrams else len(text)
    # main list
    dictionaries = []
    # populate dictionaries[][][] with levenshtein distances. 3d lists, babyyyyyyyyyyy!

    '''
    adding stuff to dictionaries[][][] list
    []
    [[]] i
    [[[]]] j
    [[[3]]] k
    '''
    # word
    for i in range(len(corrections)):
        # makes list 2D
        dictionaries.append([])

        # pass
        for j in range(passes):
            # makes list 3D
            dictionaries[i].append([])

            # need to start from 1 because can't take ngrams of 0
            ngrams = ngramify(word_level_ngrams, text, j + 1)

            index_1 = 0
            index_2 = 0

            for k in range(len(ngrams)):
                # note: index_1 and index_2 are TEXT-LEVEL INDICES, not ngram-level
                # indices, meaning they refer to character positions in the full text.
                # this is because it makes much easier collision checking for
                # word-level ngrams & text replacement at the end

                if word_level_ngrams:
                    '''
                    'one two three four five'

                    'one two three'
                    index_1 = 0
                    index_2 = index_1 + len('one two three') == 12

                    'two three four'
                    index_1 += len('one ') == 4
                    index_2 = index_1 + len('two three four') == 17

                    'three four five'
                    index_1 += len('one two ') == 8
                    index_2 = index_1 + len('three four five') == 22
                    '''
                    if k > 0:
                        previous_ngram_split = ngrams[k - 1].split()
                        previous_word = previous_ngram_split[0]
                        # + 1 to add the space ' '
                        index_1 += len(previous_word) + 1

                    ngram_split = ngrams[k].split()

                    index_2 = index_1 + len(ngrams[k]) - 1

                # much simpler if character-level ngrams!
                else:
                    index_1 = k
                    index_2 = k + j

                #print('index_1:', str(index_1) + '; index_2', index_2)

                dictionaries[i][j].append({'pass': j,
                                           'correction': corrections[i],
                                           'ngram': ngrams[k],
                                           'index_1': index_1,
                                           'index_2': index_2,
                                           'distance': distance(corrections[i], ngrams[k]),
                                           'substitute': False,
                                           'review': False})

                #print(dictionaries[i][j][k])

            '''
            ~~~ HORIZONTAL (across n-grams) CHECK ~~~

            all correct substitutions have higher distances on either side of them
            (ie they're each a local minimum over 3 values)

            for example, here are some console prints. look at the 'distance' property

            character-level n-grams
            {'pass': 1, 'correction': 'two', 'ngram': ' t', 'index_1': 3, 'index_2': 4, 'distance': 3, 'meets_threshold': False}
            {'pass': 1, 'correction': 'two', 'ngram': 'tw', 'index_1': 4, 'index_2': 5, 'distance': 1, 'meets_threshold': False}
            {'pass': 1, 'correction': 'two', 'ngram': 'w ', 'index_1': 5, 'index_2': 6, 'distance': 2, 'meets_threshold': False}

            word-level n-grams
            {'pass': 1, 'correction': 'two three', 'ngram': 'one tw', 'index_1': 0, 'index_2': 1, 'distance': 7, 'meets_threshold': False}
            {'pass': 1, 'correction': 'two three', 'ngram': 'tw three', 'index_1': 1, 'index_2': 2, 'distance': 1, 'meets_threshold': False}
            {'pass': 1, 'correction': 'two three', 'ngram': 'three four', 'index_1': 2, 'index_2': 3, 'distance': 9, 'meets_threshold': False}

            {'pass': 1, 'correction': 'five six', 'ngram': 'four fiv', 'index_1': 3, 'index_2': 4, 'distance': 5, 'meets_threshold': False}
            {'pass': 1, 'correction': 'five six', 'ngram': 'fiv six', 'index_1': 4, 'index_2': 5, 'distance': 1, 'meets_threshold': False}
            {'pass': 1, 'correction': 'five six', 'ngram': 'six seven', 'index_1': 5, 'index_2': 6, 'distance': 7, 'meets_threshold': False}

            this is a necessary, but not sufficient, condition. the substitution threshold is used to further narrow down results

            note that with start and end values we don't actually know if they satisfy this condition or not,
            so we have to make do with only checking one side

            so eg the asterisked values here are the ones that should go through to the threshold check
            2*, 3, 2*, 5, 4, 4, 2*, 3, 4, 7, 6*, 7, 6*, 7, 8, 7*
            '''
            for k in range(len(ngrams)):
                dict = dictionaries[i][j][k]

                lower_distance = dictionaries[i][j][k - 1]['distance'] if k > 0 \
                                 else None
                middle_distance = dict['distance']
                upper_distance = dictionaries[i][j][k + 1]['distance'] if \
                                 k < len(ngrams) - 1 else None
                local_minimum = False

                # otherwise index out of bounds
                if len(ngrams) > 1:
                    if k == 0:
                        if middle_distance < upper_distance:
                            local_minimum = True
                            #print('local_minimum:', str(middle_distance) + '*', str(upper_distance) + '; correction:', dictionaries[i][j][k]['correction'] + '; ngram:', dictionaries[i][j][k]['ngram'])
                    elif k == len(ngrams) - 1:
                        if middle_distance < lower_distance:
                            local_minimum = True
                            #print('local_minimum:', lower_distance, str(middle_distance) + '*; correction:', dictionaries[i][j][k]['correction'] + '; ngram:', dictionaries[i][j][k]['ngram'])
                    else:
                        if(middle_distance < upper_distance and
                           middle_distance < lower_distance):
                            local_minimum = True
                            #print('local_minimum:', lower_distance, str(middle_distance) + '*', str(upper_distance) + '; correction:', dictionaries[i][j][k]['correction'] + '; ngram:', dictionaries[i][j][k]['ngram'])
                # if single value we have no choice but to check
                else:
                    local_minimum = True
                    #print('local_minimum:', str(middle_distance) + '; correction:', dictionaries[i][j][k]['correction'] + '; ngram:', dictionaries[i][j][k]['ngram'])

                if local_minimum and middle_distance <= substitution_threshold \
                   and dict['distance'] > 0:
                    dictionaries[i][j][k]['substitute'] = True
                    output_message('TO SUBSTITUTE', dict)

    print('')

    '''
    ~~~ COLLISION CHECK ~~~
    for each ngram labelled 'substitute':
        - check whether it clashes with any other ngrams
        - if it does, compare their distances
        - set 'substitute' to false for the one with the higher distance
    '''
    for i in range(len(dictionaries)):
        for j in range(len(dictionaries[i])):
            for dict_1 in dictionaries[i][j]:
                if(dict_1['substitute']):
                    for i2 in range(len(dictionaries)):
                        for j2 in range(len(dictionaries[i2])):
                            for dict_2 in dictionaries[i2][j2]:
                                # need to check dict_1 again because it may have been
                                # made not to substitute, which would generate false positives
                                if dict_1['substitute'] and dict_2['substitute'] and \
                                   ngrams_collide(dict_1, dict_2) and dict_1 != dict_2:
                                    if dict_1['distance'] > dict_2['distance']:
                                        output_message('COLLISION', dict_1, dict_2)
                                        dict_1['substitute'] = False
                                    elif dict_1['distance'] == dict_2['distance']:
                                        '''
                                        prefer not taking away spaces because otherwise
                                        words can get squashed next to each other.
                                        eg substituting 'two' for 'tw ' or 'tw' —
                                        if you substitute the first one you delete a
                                        necessary space
                                        '''

                                        if review_collisions:
                                            output_message('REVIEW', dict_1, dict_2, text)

                                            while True:
                                                match input('ENTER (1), (2), OR (3): '):
                                                    case '1':
                                                        dict_2['substitute'] = False
                                                        break
                                                    case '2':
                                                        dict_1['substitute'] = False
                                                        break
                                                    case '3':
                                                        dict_1['substitute'] = False
                                                        dict_2['substitute'] = False
                                                        break
                                        else:
                                            if dict_1['ngram'][0] == ' ' or dict_1['ngram'][-1] == ' ':
                                                output_message('COLLISION', dict_1, dict_2)
                                                dict_1['substitute'] = False
                                            else:
                                                output_message('COLLISION', dict_2, dict_1)
                                                dict_2['substitute'] = False
                                    else:
                                        output_message('COLLISION', dict_2, dict_1)
                                        dict_2['substitute'] = False

    print('')

    '''
    ~~~ FINALLY MAKE SUBSTITUTIONS! ~~~
    '''
    offset = 0

    for i in range(len(dictionaries)):
        for j in range(len(dictionaries[i])):
            for dict in dictionaries[i][j]:
                if dict['substitute']:
                    substitutions_count += 1
                    output_message('SUBSTITUTED', dict)

                    to_replace = text[: dict['index_1'] + offset] + dict['correction'] + \
                                 text[dict['index_2'] + 1 + offset :]
                    offset += len(to_replace) - len(text)
                    text = to_replace

    print('\ninput:', text_to_correct)
    print('output:', text)
    print('number of substitutions:', substitutions_count)

'''
note: using character-level-ngrams is pretty much useless, because they would
only be more useful than word-level ones for situations where words need to be
split apart, like 'sxvenand' -> 'seven and'. however, without ai, it is impossible
to distinguish between whether you should delete the extra characters (the 'and'
in this case), or add a space and keep them.

but mostly, it's cause they fail so spectacularly. due to not looking at whole
words, they keep in characters they should be deleting.

but i'm still keeping the option in the code as it may end up being useful later.
'''
word_level_ngrams = False
substitution_threshold = 1
review_threshold = 3
review_collisions = True
#text = 'one tw three four fiv six seven eigh nine ten'
#corrections = ['two', 'two three', 'five six', 'eight nine ten']
text = 'The quicc brow nfox jump over the lazy dog, an eye for one am very happy a t these developments'
corrections = ['quick', 'brown', 'fox', 'jumps', 'and']
# correct_text = 'The quick brown fox jumps over the lazy dog, and I for one am very happy at this development'
# corrections = ['EmployAbility', 'neurodivergence', 'Next Generation Inclusive']

correct_text(word_level_ngrams, substitution_threshold, review_threshold, \
             review_collisions, text, corrections)

'''
TODO make it work with multiline files, by adding another for loop for lines
TODO add this script to the web app, as a separate feature to the file conversion.
     make the options and review stage graphical
(TODO add a 'review' stage, where it tells you changes it was unsure about
      and gives you option to either revert them, keep them, or input a custom string
      — although i don't know if there are any changes it's unsure about, and
      how to determine them if there's not. that's why this's a last thing to do)
'''
