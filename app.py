import os
import pathlib
from flask import Flask, render_template, request, send_from_directory
from werkzeug.utils import secure_filename
from file_converter import *

app = Flask(__name__)

@app.route('/')
def index():
    # don't want random files hanging around in uploads/downloads folders
    for file in os.listdir('uploads'):
        os.remove('uploads/' + file)

    for file in os.listdir('downloads'):
        os.remove('downloads/' + file)

    return render_template('index.html')

# triggered after user hits submit button to upload a file
@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        filename = file.filename
        filesystem_filename = secure_filename(filename)
        file_extension = pathlib.Path(filename).suffix

        if file_extension == '.txt':
            convert_from = 'Text'
        elif file_extension == '.vtt':
            convert_from = 'WebVTT'
        elif file_extension == '.srt':
            convert_from = 'SubRip'
        else:
            return render_template('index.html', invalid_filetype='True')

        file.save('uploads/' + filesystem_filename)

        # need to use config dictionary for global variables because otherwise
        # they don't persist across routes
        app.config['filesystem_filename'] = filesystem_filename
        app.config['convert_from'] = convert_from

        return render_template('upload.html', filename=filename, convert_from=convert_from)
    else:
        return 'Error: invalid request method'

# triggered after user clicks a convert button in upload.html
@app.route('/download', methods=['GET', 'POST'])
def download():
    if request.method == 'POST':
        # delete current files in downloads folder to avoid any wrong downloads
        for file in os.listdir('downloads'):
            os.remove('downloads/' + file)

        filesystem_filename = app.config['filesystem_filename']
        convert_from = app.config['convert_from']
        convert_to = request.form['convert_button']

        if convert_from == 'Text':
            if convert_to == 'WebVTT':
                # filename, input directory, output directory, output type
                convert_from_txt(filesystem_filename, 'uploads', 'downloads', 'vtt')
            elif convert_to == 'SubRip':
                convert_from_txt(filesystem_filename, 'uploads', 'downloads', 'srt')
            else:
                return 'Error: invalid type to convert to'
        elif convert_from == 'WebVTT':
            if convert_to == 'Text':
                convert_from_vtt(filesystem_filename, 'uploads', 'downloads', 'txt')
            elif convert_to == 'SubRip':
                convert_from_vtt(filesystem_filename, 'uploads', 'downloads', 'srt')
            else:
                return 'Error: invalid type to convert to'
        elif convert_from == 'SubRip':
            if convert_to == 'WebVTT':
                convert_srt_to_vtt(filesystem_filename, 'uploads', 'downloads')
            else:
                return 'Error: invalid type to convert to'
        else:
            return 'Error: Invalid type to convert from'

        # should only ever be 1 file in downloads folder; if there are more, this won't work
        return send_from_directory('downloads', os.listdir('downloads')[0], as_attachment=True)
    else:
        return 'Error: Invalid request method'

if __name__ == '__main__':
    app.run()
