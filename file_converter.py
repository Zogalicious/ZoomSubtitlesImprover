# possible conversions:
# • Text (with timestamps) -> WebVTT
# • Text (with timestamps) -> SubRip
# • WebVTT -> Text (without timestamps)
# • WebVTT -> SubRip
# • SubRip -> WebVTT
# the reason the Text output is without timestamps is some people want full transcripts of the sessions

from pathlib import Path
from re import search

# class expecting to receive hours, minutes, seconds as ints
class Timestamp:
    hours = 0
    minutes = 0
    seconds = 0

    def __init__(self, hours, minutes, seconds):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    # convert current int format of timestamp to string, eg '14:56:19'
    def string_representation(self):
        string_hours = '00'
        string_minutes = '00'
        string_seconds = '00'

        # to get eg '09' rather than '9'

        if self.hours < 10:
            string_hours = '0' + str(self.hours)
        else:
            string_hours = str(self.hours)

        if self.minutes < 10:
            string_minutes = '0' + str(self.minutes)
        else:
            string_minutes = str(self.minutes)

        if self.seconds < 10:
            string_seconds = '0' + str(self.seconds)
        else:
            string_seconds = str(self.seconds)

        return string_hours + ':' + string_minutes + ':' + string_seconds + '.000'

def subtract_timestamps(timestamp1, timestamp2):
    hours = timestamp1.hours - timestamp2.hours
    minutes = timestamp1.minutes - timestamp2.minutes
    seconds = timestamp1.seconds - timestamp2.seconds

    # logic to deal with, eg, '17:02:33' - '16:05:54', to get '00:56:39' rather than '01:-03:-21'
    # structure of the logic:
    # 17:02:33 is timestamp1
    # 16:05:54 is timestamp2
    # 01:-03:-21 => 00:56:39 is the conversion we wanna make
    # => 00:57:-21 after checking minutes
    # => 00:56:39 after checking seconds

    # hours edge case on starting a new day (23:xx:xx - 00:xx:xx)
    if hours < 0:
        hours += 24

    if minutes < 0:
        hours -= 1
        # '+' not '-' because hours is already negative
        minutes += 60

    if seconds < 0:
        minutes -= 1
        seconds += 60

    return Timestamp(hours, minutes, seconds)

def add_timestamps(timestamp1, timestamp2):
    # (x - 0) - (0 - y) = x + y
    return subtract_timestamps(
           subtract_timestamps(timestamp1, Timestamp(0, 0, 0)),
           subtract_timestamps(Timestamp(0, 0, 0), timestamp2))

def timestamp_from_string(string_timestamp):
    return Timestamp(int(string_timestamp[0:2]), int(string_timestamp[3:5]), int(string_timestamp[6:8]))

def convert_srt_to_vtt(srt_filename, input_directory, output_directory):
    with open(input_directory + '/' + srt_filename) as srt_file:
        srt_lines = srt_file.readlines()

    vtt_filename = Path(srt_filename).with_suffix('.vtt')
    with open(output_directory + '/' + vtt_filename, "w") as vtt_file:
        vtt_file.write("WEBVTT\n\n\n") # Intial VTT file header

        # Every 4th line of the SRT file is timestamp or text
        for i in range(len(srt_lines)):
            if i % 4 == 1:
                vtt_file.write(srt_lines[i].replace(",", "."))
                vtt_file.write(srt_lines[i+1])
                vtt_file.write("\n")

# Takes a TXT file and the file format you want to convert it to
def convert_from_txt(txt_filename, input_directory, output_directory, output_type):
    # initialise lists which will be used to fill out timestamps and rest of text in vtt file. each entry corresponds to each line in text file
    string_timestamps = []
    text_lines_excluding_timestamps = []

    with open(input_directory + '/' + txt_filename) as txt_file:
        # list of all lines in the text file
        text_lines = txt_file.readlines()
        # RegEx search. need to do .group() to convert from RegEx Match object to string
        string_initial_timestamp = search('\d\d:\d\d:\d\d', text_lines[0]).group()
        initial_timestamp = timestamp_from_string(string_initial_timestamp)

        # should fill out timestamps list with eg [00:00:00.000, 00:00:10.000, 00:00:13.000, 00:00:28.000, etc]
        for line in text_lines:
            string_current_timestamp = search('\d\d:\d\d:\d\d', line).group()
            current_timestamp =  timestamp_from_string(string_current_timestamp)
            difference_from_initial_timestamp = subtract_timestamps(current_timestamp, initial_timestamp)
            string_timestamps.append(difference_from_initial_timestamp.string_representation())
            text_lines_excluding_timestamps.append(line[9:])

        # add last timestamp. there is no last timestamp currently, so we need to create a new one
        string_last_timestamp = string_timestamps[-1]
        last_timestamp = timestamp_from_string(string_last_timestamp)
        # add arbitrarily large number of seconds to final subtitle – it doesn't matter much if it overruns
        # because the video's gonna end anyway, and we don't want it to underrun
        last_timestamp = add_timestamps(last_timestamp, Timestamp(0, 0, 20))
        string_last_timestamp = last_timestamp.string_representation()
        string_timestamps.append(string_last_timestamp)

    # writes file info into SRT Format
    if output_type == "srt":
        srt_filename = Path(txt_filename).with_suffix('.srt')
        with open(output_directory + '/' + srt_filename, "w") as srt_file:
            # millsecond separator is "," as opposed to "."
            string_timestamps[0] = "00:00:00,000"
            for i in range(len(string_timestamps) - 1):
                string_timestamps[i+1] = string_timestamps[i+1].replace(".", ",")
                srt_file.write(str(i+1)) # Subtitle Numbers are  1-indexed
                srt_file.write("\n")
                srt_file.write(string_timestamps[i] + " --> " + string_timestamps[i + 1])
                srt_file.write("\n")
                srt_file.write(text_lines_excluding_timestamps[i])
                srt_file.write("\n")

    # writes file info in VTT format
    elif output_type == "vtt":
        vtt_filename = Path(txt_filename).with_suffix('.vtt')
        with open(output_directory + '/' + vtt_filename, "w") as vtt_file:
            vtt_file.write("WEBVTT\n\n\n") # Intial VTT file header
            for i in range(len(string_timestamps) - 1):
                vtt_file.write(string_timestamps[i] + " --> " + string_timestamps[i + 1])
                vtt_file.write("\n")
                vtt_file.write(text_lines_excluding_timestamps[i])
                vtt_file.write("\n")

# Takes a VTT file and the file format you want to convert it to.
# note that, when converting to text, we are not including timestamps,
# whereas, when converting from text, the original text file does have timestamps,
# thus the conversion is "non-aligned"
def convert_from_vtt(vtt_filename, input_directory, output_directory, output_type):
    with open(input_directory + '/' + vtt_filename) as vtt_file:
        vtt_lines = vtt_file.readlines()
        # Delete VTT file header
        for i in range(3):
            del vtt_lines[0]

        # Every 3rd line of the VTT file is either a timestamp or text line
        # Write into SRT Format
        if output_type == "srt":
            srt_filename = Path(vtt_filename).with_suffix('.srt')
            with open(output_directory + '/' + srt_filename, "w") as srt_file:
                for i in range(len(vtt_lines)):
                    if i % 3 == 0:
                        # line numbering & timestamp
                        srt_file.write(str(int(i/3 + 1)))
                        srt_file.write("\n")
                        srt_file.write(vtt_lines[i].replace(".", ","))
                        # text line
                        srt_file.write(vtt_lines[i+1])
                        srt_file.write("\n")

        # Write into TXT Format
        elif output_type == "txt":
            txt_filename = Path(vtt_filename).with_suffix('.txt')
            with open(output_directory + '/' + txt_filename, "w") as txt_file:
                for i in range(len(vtt_lines)):
                    if i % 3 == 0:
                        txt_file.write(vtt_lines[i+1])
